	({
	
	className: 'alert-notification tcenter',
	
	self: null,
	
	initialize: function(options) {
		this._super("initialize", [options]);	//render() する為のおまじない
		this.self = this;	//退避
		
		//collection を監視（BackBone上でsyncイベントが発火した時にgetUnReadDataを実行）
		this.collection.on("sync", this.getUnReadData, this);
		
	},
	
	//未読データ取得
	getUnReadData: function(){
		var self = this.self;
		
		//URL生成
		var rssUrl = app.api.buildURL('get/UnReadData',null,null,null);
		//api呼び出し
		app.api.call('get', rssUrl, null,{
			success: function (data) {
				
				//data: php側の戻り値
				self.alert_count = data.alert_count;	//未読&重要データ件数
				self.unread_count = data.unread_count;	//未読データ件数
				
				self.render();
			},
		});
	},
	
	})