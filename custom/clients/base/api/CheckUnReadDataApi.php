<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('data/BeanFactory.php');
require_once('include/api/SugarApi.php');
require_once('modules/Notifications/Notifications.php');

class CheckUnReadDataApi extends SugarApi
{
	public function registerApiRest()
	{
		return array(
			'GetUnReadData' => array(
				//request type
				'reqType' => 'GET',
				//endpoint path
				'path' => array('get', 'UnReadData'),
				//endpoint variables
				'pathVars' => array(''),
				//method to call
				'method' => 'getUnreadData',
				//short help string to be displayed in the help documentation
				'shortHelp' => '',
				//long help to be displayed in the help documentation
				'longHelp' => '',
			),
			'CheckUnReadData' => array(
				//request type
				'reqType' => 'GET',
				//endpoint path
				'path' => array('check', 'UnReadData'),
				//endpoint variables
				'pathVars' => array(''),
				//method to call
				'method' => 'checkUnreadData',
				//short help string to be displayed in the help documentation
				'shortHelp' => '',
				//long help to be displayed in the help documentation
				'longHelp' => '',
			),
		);
	}
 
	public function getUnreadData($api, $args)
	{
		$user_id = $GLOBALS['current_user']->id;
		$alert_count = 0;
		$unread_count = 0;
		
		//モジュールインスタンス
		$notificate = new Notifications();
		//グローバルdb
		global $db;
		
		//未読&アラートデータ件数 sql生成
		$select = "select count(*) from notifications ";
		$where_alert = sprintf("where notifications.assigned_user_id = '%s' and is_read ='0' and severity = 'Alert' and deleted = '0'", $user_id);
		$alert_count_sql = $select.$where_alert;
		
		//sql実行
		$alert_result = $notificate->db->query($alert_count_sql, true);
		if($alert_result){
			$alert_record = $db->fetchByAssoc($alert_result);
			$alert_count = $alert_record['count(*)'];
		}
		
		
		//未読件数 sql生成
		$where_unread = sprintf("where notifications.assigned_user_id = '%s' and is_read ='0' and deleted = '0'", $user_id);
		$unread_count_sql = $select.$where_unread;
		
		//sql実行
		$unread_result = $notificate->db->query($unread_count_sql, true);
		if($unread_result){
			$unread_record = $db->fetchByAssoc($unread_result);
			$unread_count = $unread_record['count(*)'];
		}
		
		return array("alert_count" => $alert_count, "unread_count" => $unread_count);
	}
	
	public function checkUnreadData($api, $args){
		
		$user_id = $GLOBALS['current_user']->id;
		//モジュールインスタンス
		$notificate = new Notifications();
		
		//未読データチェック
		$order = "";
		$where = sprintf("notifications.assigned_user_id = '%s' and is_read ='0'", $user_id);
		$notificate_list = $notificate->get_full_list($order, $where);
		if($notificate_list){
			return "success";		//#Notification へ
		}else{
			return "error";			//#Home へ
		}
		
	}
}
 
?>