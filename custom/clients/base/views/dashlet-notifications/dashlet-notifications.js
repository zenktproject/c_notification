/*
 * custom dashlet-notification
 */

({
    plugins: ['Dashlet'],
	
	self: null,

	initDashlet: function() {
		this.self = this;	//退避
		this.getUnReadData();
    },
	
	//未読データ取得
	getUnReadData: function(){
		console.log("getUnReadData - dashlet!!");
		var self = this.self;
		
		//URL生成
		var rssUrl = app.api.buildURL('get/UnReadData',null,null,null);
		//api呼び出し
		app.api.call('get', rssUrl, null,{
			success: function (data) {
				
				//data: php側の戻り値
				console.log("unReadDataApi - success");
				console.log(data);
				self.alert_count = data.alert_count;	//未読&重要データ件数
				self.unread_count = data.unread_count;	//未読データ件数
				
				self.render();
			},
		});
	},
})
