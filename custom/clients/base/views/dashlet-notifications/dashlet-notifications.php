<?php

/*
 * custom dashlet-notifications
 */

$viewdefs['base']['view']['dashlet-notifications'] = array(
    'dashlets' => array(
        array(
			'label' => '未読通知件数',
			'description' => '未読の通知件数を表示',
            'config' => array(
            ),
            'preview' => array(
            ),
        ),
    ),
    'config' => array(
    ),
);
